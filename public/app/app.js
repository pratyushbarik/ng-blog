var app = angular.module('blogApp', [
  'ngRoute'
])

app.config(['$routeProvider', function($routeProvider) {

  $routeProvider
    .when('/', {
      template: '<h1>You are logged in!</h1>',
      controller: 'DashboardController'
    })
    .when('/listing', {
      templateUrl: 'app/listing.html',
      controller: 'ListingController'
    })
    .when('/create', {
      templateUrl: '/app/addPost.html',
      controller: 'NewPostController'
    })
    .otherwise({
      redirectTo: '/'
    })

}])

.controller('DashboardController', ['$scope', function($scope) {

}])
.controller('ListingController', ['$scope', 'PostServ', function($scope, PostServ) {
  $scope.posts = []

  PostServ.list()
  .then(function(response) {
    $scope.posts = response.data.post
  }, function(error) {})

}])
.controller('NewPostController', ['$scope', 'PostServ', function($scope, PostServ) {
  var url = 'http://localhost:8000/post'
  $scope.newPost = {}
  $scope.message = ''

  $scope.addNewPost = function() {
    PostServ.create($scope.newPost)
      .then(function(response) {
        $scope.message = "Post created successfully"
      }, function(error) {})
  }

  $scope.hideMessage = function() {
    $scope.message = ''
  }
}])
.directive('notification', function() {
  return {
    template: '<p>{{message}}</p><a ng-click="hideMessage()">Hide</a>',
    scope: {
      message: '='
    },
    controller: function($scope) {
      $scope.hideMessage = function() {
        //make the message string emtpy
        $scope.message = ''
      }
    }
  }
})
.service('PostServ', ['$http', function($http) {
  var url = "http://localhost:8000/post"

  this.list = function() {
    return $http.get(url)
  }

  this.create = function(payload) {
    return $http.post(url, payload)
  }
}])

.factory('PostFact', ['$http', function($http) {
  return {
    url: '',
    list: function() {},
    create: function() {}
  }
}])


