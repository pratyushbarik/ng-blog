@extends('layouts.app')

@section('content')
<div class="container" ng-app="blogApp">
    <div class="row" ng-view></div>
</div>
@endsection
