# A Micro-blogging app to demonstrate Angular with Laravel

Please follow the steps after cloning the repo.

1. Make a copy of `.env.example` file in the root directory and name it `.env`

2. Set up your database in the `.env` file by configuring  `DB_HOST`=127.0.0.1, `DB_DATABASE`=**db_name**, `DB_USERNAME`=**username**,`DB_PASSWORD`=**password**

3. Install `composer` globally and execute `composer install` in the root directory

4. Now execute `php artisan serve` and you can access the server on `localhost:8000`