<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $hidden = ['created_at', 'deleted_at'];

    protected $fillable = ['title', 'body', 'image'];

    public function setImageAttribute($value)
    {
      $this->attributes['image'] = 'http://lorempixel.com/400/200/city/' . rand(1,9);
    }
}
